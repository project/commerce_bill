Commerce Bill
=================

It uses the commerce order entity to hold invoice information.

## Setup

1. Install the module.

2. Edit your product variation type and enable the 'Shippable' trait

3. Edit your order type:
  - Select one of the fulfilment workflows.
  - Enable shipping and choose a shipment type.
  - Select the 'Shipping' checkout flow
