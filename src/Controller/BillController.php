<?php

namespace Drupal\commerce_bill\Controller;


use Drupal\Core\Controller\ControllerBase;

class BillController extends ControllerBase {

  /**
   * Returns a simple page.
   *
   * @param null $commerce_order
   *
   * @return array
   *   A simple renderable array.
   */
  public function myBill($commerce_order = NULL) {
    $element = [
      '#markup' => $commerce_order
    ];

    return $element;
  }

}